//
//  ShowExhibitionDetailViewController.swift
//  Pier2
//
//  Created by yang on 2018/12/1.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class ExhibitionDetailViewController: UIViewController {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var ticketInfoTextView: UITextView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var exhibitionInfoLabel: UILabel!
    
    var selectedExhibition:Exhibition!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = selectedExhibition.name
        coverImageView.image = selectedExhibition.image_cover
        dateLabel.text = selectedExhibition.exhibiteDate
        timeLabel.text = selectedExhibition.openingTime
        ticketInfoTextView.text = selectedExhibition.ticketInfo
        locationLabel.text = selectedExhibition.location
        exhibitionInfoLabel.text = selectedExhibition.exhibitionInfo
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
