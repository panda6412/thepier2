//
//  ExhibitionImagesPageViewController.swift
//  Pier2
//
//  Created by yang on 2018/11/25.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class ExhibitionImagesPageViewController: UIPageViewController {

    var images:[UIImage]?
    var exhibitions:[Exhibition] = ExhibitonCategory.getAllExhibitions()
    
    weak var pageViewControllerDelegate: ExhibitionImagesPageViewControllerDelegate?
    
    struct StoryBoard {
        static let exhibitionImageViewController = "ExhibitionImageViewController"
    }
    
    lazy var controllers:[UIViewController] = {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var controllers = [UIViewController]()
        print(exhibitions)
        var page:Int = Int(ceil(Double(exhibitions.count)/3))
        print(" >> page : \(page)")
        
        for i in 0..<page {
            let exhibitionImageVC = storyboard.instantiateViewController(withIdentifier: StoryBoard.exhibitionImageViewController)
            controllers.append(exhibitionImageVC)
        }
        
        self.pageViewControllerDelegate?.setupPageController(numberOfPages: controllers.count)
        print(controllers)
        return controllers
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("ExhibitionImagesPageViewController view didload")
        dataSource = self
        delegate = self

        self.turnToPage(index: 0)
        
    }
    
    func turnToPage(index: Int){
        let controller = controllers[index]
        var direction = NavigationDirection.forward
        if let currentVC = controllers.first {
            let currentIndex = controllers.index(of: currentVC)
            if currentIndex! > index {
                direction = .reverse
            }
        }
        
        self.configureDisplaying(viewController: controller)
        setViewControllers([controller], direction: direction, animated: true, completion: nil)
    }
    
    func configureDisplaying(viewController: UIViewController) {
        for (index, vc) in controllers.enumerated() {
            if viewController === vc {
                if let exhibitionImageVC = viewController as? ExhibitionImageViewController {
                    
                    exhibitionImageVC.image1 = self.exhibitions[index*3].image_cover
                    if(index == 8){
                        return
                    }
                    exhibitionImageVC.image2 = self.exhibitions[index*3+1].image_cover
                    exhibitionImageVC.image3 = self.exhibitions[index*3+2].image_cover
                }
                
                self.pageViewControllerDelegate?.turnPageController(to: index)
            }
        }
    }
}

protocol ExhibitionImagesPageViewControllerDelegate: class {
    func setupPageController(numberOfPages : Int)
    func turnPageController(to index: Int)
}

// MARK: - UIPageViewController DataSource

extension ExhibitionImagesPageViewController : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let index = controllers.index(of: viewController) {
            if index > 0 {
                return controllers[index - 1]
            }
        }
        return controllers.last
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let index = controllers.index(of: viewController) {
            if index < controllers.count - 1  {
                return controllers[index + 1]
            }
        }
        return controllers.first
    }
}

// MARK: - UIPageViewController Delegate

extension ExhibitionImagesPageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.configureDisplaying(viewController: pendingViewControllers.first as! ExhibitionImageViewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !finished{
            self.configureDisplaying(viewController: previousViewControllers.first as! ExhibitionImageViewController)
        }
    }
}
