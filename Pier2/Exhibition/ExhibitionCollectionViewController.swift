//
//  ExhibitionCollectionViewController.swift
//  Pier2
//
//  Created by yang on 2018/12/1.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class ExhibitionCollectionController : UICollectionViewController {
    var allExhibitions:[ExhibitonCategory] = ExhibitonCategory.getAllExhitions()
    var selectedExhibition:Exhibition!
    
    struct Storyboard {
        static let exhibitionCell = "exhibitionCell_reular"
        static let sectionHeaderView = "SectionHeaderView"
        static let showExhibitionDetailVC = "ShowExhibitionDetail"
    }
    // MARK - UICollectionViewController lifecycle here

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO - change the layoutof the collection view
    }
    
    //MARK - UICollectionViewDatasource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return allExhibitions.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allExhibitions[section].exhibitions.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.exhibitionCell, for: indexPath) as! ExhibitionCell
        
        let exhibitionCategory:ExhibitonCategory = allExhibitions[indexPath.section]
        let exhibition:Exhibition = exhibitionCategory.exhibitions[indexPath.item]
        
        let exhibition_square_image = exhibition.image_square
        cell.exhibitionImage = exhibition_square_image
        cell.infoImage = chooseInfoImage(by: exhibition.ticketDescription)
        cell.infoDescriptionStr = "[" + exhibition.ticketDescription + "]"

        return initCellStyle(on: cell)
    }
    
    
    // Section header view
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Storyboard.sectionHeaderView, for: indexPath) as! SectionHeaderView
        
        let exhibitionCategory:ExhibitonCategory = allExhibitions[indexPath.section]
        sectionHeaderView.catgoryTitle = exhibitionCategory.category.rawValue
        
        return sectionHeaderView
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let exhibitionCategory:ExhibitonCategory = allExhibitions[indexPath.section]
        selectedExhibition = exhibitionCategory.exhibitions[indexPath.item]
        performSegue(withIdentifier: Storyboard.showExhibitionDetailVC, sender: nil)
    }
    
    
    // MARK - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Storyboard.showExhibitionDetailVC {
            let exhibitionDetailVC = segue.destination as! ExhibitionDetailViewController
            exhibitionDetailVC.selectedExhibition = selectedExhibition
        }
    }
    
    func chooseInfoImage(by ticketDescription:String) -> UIImage {
        var infoImageName:String
        switch ticketDescription {
        case "免費":
            infoImageName = "ticket_info_free"
        case "公告":
            infoImageName = "ticket_info_announcement"
        case "售票":
            infoImageName = "ticket_info_ticket"
        default:
            infoImageName = "ticket_info_default"
        }
        return UIImage(named: infoImageName)!
    }
    
    func initCellStyle(on cell:ExhibitionCell) -> ExhibitionCell {
        
        let exhibitionImageView = cell.exhibitionImageView!
        exhibitionImageView.layer.cornerRadius = exhibitionImageView.layer.frame.width/10
        exhibitionImageView.layer.masksToBounds = true
        exhibitionImageView.layer.shadowColor = UIColor.red.cgColor
        exhibitionImageView.layer.shadowRadius = 4
        exhibitionImageView.layer.shadowOpacity = 0.6
        exhibitionImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        return cell
    }
}
