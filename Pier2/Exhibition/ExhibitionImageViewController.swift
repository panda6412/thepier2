//
//  ExhibitionImageViewController.swift
//  Pier2
//
//  Created by yang on 2018/11/25.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class ExhibitionImageViewController: UIViewController {

    @IBOutlet weak var imageView1:UIImageView?
    @IBOutlet weak var imageView2:UIImageView?
    @IBOutlet weak var imageView3:UIImageView?
    
    var image1:UIImage? {
        didSet {
            self.imageView1?.image = image1
            
            self.imageView1?.layer.cornerRadius = self.imageView1!.bounds.width/15
            self.imageView1?.layer.masksToBounds = true
        }
    }
        
    var image2:UIImage? {
        didSet {
            self.imageView2?.image = image2
            
            self.imageView2?.layer.cornerRadius = self.imageView2!.bounds.width/15
            self.imageView2?.layer.masksToBounds = true
        }
    }
    
    var image3:UIImage? {
        didSet {
            self.imageView3?.image = image3
            
            self.imageView3?.layer.cornerRadius = self.imageView3!.bounds.width/15
            self.imageView3?.layer.masksToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ExhibitionImageViewController view didload")
        self.imageView1?.image = image1
        self.imageView2?.image = image2
        self.imageView3?.image = image3
    }
}
