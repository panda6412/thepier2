//
//  ExhibitonCategory.swift
//  Pier2
//
//  Created by yang on 2018/11/25.
//  Copyright © 2018 yang. All rights reserved.
//

import Foundation

enum Category:String {
    case daYoung = "大勇區"
    case daYi = "大義區"
    case ponglai = "蓬萊區"
    case experient = "體驗區"
    case exhibition = "展覽演出"
    case market = "市集"
}

class ExhibitonCategory {
    var category:Category
    var exhibitions:[Exhibition]
    
    init(category:Category, exhibitions:[Exhibition]) {
        self.category = category
        self.exhibitions = exhibitions
    }
    
    class func getAllExhitions() -> [ExhibitonCategory] {
        return [daYoungExhition(), daYiExhibition(), pongLaiExhibition(), experientExhibition(), exhibition(), marketExhibition()]
    }
    
    class func getAllExhibitions() -> [Exhibition] {
        let exhibitiosCategory: [ExhibitonCategory] = getAllExhitions()
        var all_exhibitions:[Exhibition] = [Exhibition]()
        for exhibitions in exhibitiosCategory {
            for exhibition in exhibitions.exhibitions {
                all_exhibitions.append(exhibition)
            }
        }
        
        return all_exhibitions
    }
    
    class func getExhibition(with category: Category) -> [ExhibitonCategory] {
        switch category {
            case .daYoung:
                return [daYoungExhition()]
            case .daYi:
                return [daYiExhibition()]
            case .ponglai:
                return [pongLaiExhibition()]
            case .experient:
                return [experientExhibition()]
            case .exhibition:
                return [exhibition()]
            case .market:
                return [marketExhibition()]
            default:
                return [daYoungExhition(), daYiExhibition(), pongLaiExhibition(), experientExhibition(), exhibition(), marketExhibition()]
        }
    }
    
    // MARK: - private Helper method
    private class func daYoungExhition() -> ExhibitonCategory{
        var exhibitions = [Exhibition]()
        
        exhibitions.append(Exhibition(name: "攝影新時代—自拍狂潮", imageName_cover: "selfie_cover", imageName_square: "selfie_square", exhibitionDate: "2018-10-11~2019-01-06", openingTime: "周一至周四10:00-18:00\n周五至周日及國定假日10:00-20:00", ticketInfo: "一人一票，限同一人單日使用，展覽不限進場次數。 年齡6歲以下或身高115公分以下之幼童，與持身心障礙手冊及必要陪同者，請出示相關證明文件正本，得免票進場。\n2018高雄攝影節期10/11-10/28間\n早鳥票99元 全票149元（含P2、P3倉庫展區", ticketDescription: "售票", location: "駁二藝術特區大勇區C5倉庫", exhibitionInfo: "攝影新時代：「自拍」狂潮\nThe New Era of Photography：The Fever of “Self-portraits”\n這是一個用「自拍」來書寫歷史的時代。"))
        
        exhibitions.append(Exhibition(name: "駁二勇市集Pier-2 Market", imageName_cover: "young_market_cover", imageName_square: "young_market_cover", exhibitionDate: "2018-10-06~2018-11-25", openingTime: "每周末六日 13:00-18:00", ticketInfo: "~~~~~~~~~~~~~~免費~~~~~~~~~~~~~~", ticketDescription: "免費", location: "大勇區藝術廣場", exhibitionInfo: "［9927］手繪創意\n1+1=5手作雜貨\n1212玩樂設計\n2 Hands Waltz手作圓舞曲\nAbby's Illustration 插畫 x 手機殼訂製\nAlace 水蕾絲\nALMA の 手作人蔘\nAMIN'S SHINY WORLD\nBlack angel boutique\nBonjour！Bonheur 你好，幸福 手工天然果醬\nBubu＿studio 布布工作室\nCAT X KUMA\nciao metal design\nCOBE。手創原石\nCorotte等等各式商家"))
        
        exhibitions.append(Exhibition(name: "2018高雄藝術博覽會", imageName_cover: "art_kaohsiung_cover", imageName_square: "art_kaohsiung_square", exhibitionDate: "早鳥一日票 NT$ 100\n早鳥一日套票 NT$ 250\n團體樂透票 NT$ 510\n專刊乙本 NT$ 200", openingTime: "11/30-12/2毎日上午11:00至下午7:00，12/2\n開放至下午6:00", ticketInfo: "https://www.accupass.com/go/2018ak", ticketDescription: "售票", location: "駁二藝術特區大勇區P2、P3倉庫及城市商旅真愛館", exhibitionInfo: "ART KAOHSIUNG 高雄藝術博覽會於今年正式邁入第六屆,從 2013年開創以來,悉心耕耘台灣南部藝術市場及展業,已成功開啟台灣藝術市場新區塊。結合產官學三方力量,寫出南部藝文產業最新篇章。會展囊括中國、日本、菲律賓、香港、馬來西亞、新加坡、越南與美國近百間頂尖畫廊盛情參與。\n\n高雄藝術博覽會奠定出「東南亞及東北亞藝術的交會平台」的定位, 全力以台灣政府南進政策為地基,藝術文化交流作為前導,創立前瞻性的藝術收藏趨勢。經過六年的策略性運作及細心經營,2018年將會是高雄藝術博覽會重要的里程碑。結合高雄市政府文化局的政策遠見、各方專業經驗、網絡資源、尊榮藏家招待計畫與特別品牌聯名企劃,今年高雄藝術博覽會將展現前所未有的南部藝術實力。"))
        
        exhibitions.append(Exhibition(name: "伊藤潤二恐怖體驗展絕命逃走中 高雄站", imageName_cover: "chill_party_cover", imageName_square: "chill_party_square", exhibitionDate: "2018-12-29~2019-03-03", openingTime: "10:00~18:00（17:30最後入場）", ticketInfo: "＊單館票300元\n＊雙館票500元\n＊優待票150元（65歲以上長者、身心障礙人士本人(憑證)及陪同者一名，購票及入場時請出示相關證明文件）\n＊6歲以下幼童禁止入場；6~12歲兒童須購票，並須成人持票陪同入場。", ticketDescription: "售票", location: "大勇區自行車倉庫", exhibitionInfo: "亞洲首座重現「當代恐怖漫畫大師-伊藤潤二世界觀」的恐怖體驗展\n驚典篇章重啟新頁，再現高雄駁二！\n2018年底、跨年度，挑戰你最不想面對的感官體驗！\n\n\n【恐懼鎮、告別市，雙館齊嚇！】\n瀰漫絕望與不祥的恐懼鎮中，\n淵小姐以腥紅大嘴向你微笑，\n在巷弄盡頭等待你的，是從全身孔洞中流竄而出的絲絲寒氣；\n\n致命氣息充斥幽暗的告別市，\n雙一再度從閣樓裡敲響喪鐘，\n一道道不知通往何處的門扉，你找得到逃出這個城市的路嗎？\n\n不死不滅的絕世美人富江，邀你共享伊藤潤二漫畫的毛骨悚然。\n\n【無數相同面孔後的你，究竟是誰？】\n是要阻擋別人的窺伺？還是作為窺伺別人的保護罩？\n在雙一及富江臉孔後的，還是不是你自己？\n恐怖體驗的第一步就是戴上面具，絕命逃走的遊戲即將開始……\n\n【探索恐懼、了解恐懼，終將化身恐懼】\n與你深情對望的眼球草、經歷萬年時光沖刷的長夢結晶、女王姿態頤指氣使的富江人面疽、插滿邪氣怨念的雙一詛咒娃娃……，為你呈現伊藤潤二的世界中，所有恐懼源頭，讓驚嚇深入血脈，直達內心。"))
        
        exhibitions.append(Exhibition(name: "瘋狂泡泡實驗室 高雄站", imageName_cover: "bubble_cover", imageName_square: "bubble_square", exhibitionDate: "2018-12-29~2019-04-07", openingTime: "10:00~18:00（17:30最後入場）(除夕休館)", ticketInfo: "預售單人票220元，可至ibon售票系統、全網FamiPort、博客來售票網、GOMAJI平台購買。現場全票280元/優待票250元", ticketDescription: "售票", location: "大勇區P2倉庫", exhibitionInfo: "讓創意發泡 顛覆想像的泡泡樂園\n全台首創以泡泡為主題的情境式展覽，甩掉課本丟掉書包！科學原理簡單說！泡泡藝術超好玩！瘋狂泡泡實驗室打造大人與小孩同樂的科學藝術空間，從童趣好玩的泡泡延伸至背後的科學知識，除了擁有超好拍的夢幻打卡場景外，還有顛覆想像的泡泡樂園。\n各位科學家準備好了嗎？"))
        
        return ExhibitonCategory(category: .daYoung, exhibitions: exhibitions)
    }
    
    private class func daYiExhibition() -> ExhibitonCategory {
        var exhibitions = [Exhibition]()
        
        exhibitions.append(Exhibition(name: "鬼画連篇：臺灣動漫恐懼體驗展", imageName_cover: "ghost_cover", imageName_square: "ghost_square", exhibitionDate: "2018.9.1-12.16", openingTime: "周一至周四10:00-18:00；\n周五至周日及國定假日10:00-20:00", ticketInfo: "一人一票，限同一人單日使用，展覽不限進場次數。 年齡6歲以下或身高115公分以下之幼童，與持身心障礙手冊及必要陪同者，請出示相關證明文件正本，得免票進場。詳情請洽C5、C7售票點。\n《全日套票 - 99元，可當日無限次進出當代館、舊事倉庫及動漫倉庫展覽(鬼画連篇)\n單展票 - 50元，可單日單展無限次進出場館》", ticketDescription: "售票", location: "駁二藝術特區大義區C7動漫倉庫", exhibitionInfo: "展覽空間將有7道門，連結至漫畫家所創造的恐怖世界，恐懼題材涵蓋: 臺灣妖怪傳說、社會獵奇、女巫、凶宅、陰間、懸疑驚悚等，觀眾可探索7種不同的毛骨悚然體驗。"))
        
        exhibitions.append(Exhibition(name: "黑白畫話--哭泣的動物與人造動物", imageName_cover: "white_black_cover", imageName_square: "white_black_square", exhibitionDate: "2018-11-01~2018-11-24", openingTime: "周一至周日 12:00-18:00", ticketInfo: "~~~~~~~~~~~~~~免費~~~~~~~~~~~~~~", ticketDescription: "免費", location: "大義倉庫 C8-20漾藝廊", exhibitionInfo: "希望透過這系列畫中有話的黑白畫話，可以帶給大家對動物不同層面的思考，黑白更能呈現主角的情緒，就像黑白照片，更能聚焦。\n\n「哭泣的動物」是一系列以動物立場的創作，人類在受到傷害時會哭泣，哭泣會讓人產生同情和同理心，如果看到動物因為人類的傷害而哭泣，是否會願意給他們多點同理心？這些畫要表現的不是動物可愛，而是他們總被忽略的悲傷情緒。也藉這系列傳達拒絕皮草，抵制動物娛樂的理念。\n\n「人造動物」是畫未來人類與動物的想像。在未來人類殺光了大部分動物，只好製造機器動物，並在這些人造動物中加入了人類的喜好，讓動物明正言順的替人類服務，但人類小看了大自然的力量，這些動物體內像是有種子般發了芽長了樹枝，也因此被丟棄..."))
        
        exhibitions.append(Exhibition(name: "駁二舊事倉庫", imageName_cover: "antique_warehouse_cover", imageName_square: "antique_warehouse_square", exhibitionDate: "常設展", openingTime: "周一至周四10am~6pm，\n周五至周日及國定假日10am~8pm", ticketInfo: "一人一票，限同一人單日使用，展覽不限進場次數。\n年齡6歲以下或身高115公分以下之幼童，與持身心障礙手冊及必要陪同者，請出示相關證明文件正本，得免票進場。", ticketDescription: "售票", location: "駁二藝術特區大義C7倉庫", exhibitionInfo: "集結舊事鑑往知來──\n從福爾摩沙、打狗、鹽埕到駁二，自漁鹽、製糖、工業到藝文，將這一塊濱海地帶的人文歷史與核心價值，以各種創意手法濃縮在駁二「舊事倉庫」裡。以地洞影片為駁二歷史揭開序幕，帶我們回到三百年前的福爾摩沙小漁村，在海底洋流的空間氛圍中，打狗的發展一幕幕以新舊交融的影像出現眼前。"))
        
        exhibitions.append(Exhibition(name: "HIStory 主題設定店展", imageName_cover: "history_theme_cover", imageName_square: "history_theme_square", exhibitionDate: "2018-08-18~2018-11-30", openingTime: "週一-週日11:00-21:30", ticketInfo: "低消任一商品/每人", ticketDescription: "公告", location: "大義區C9-18倉庫", exhibitionInfo: "為了回饋所有喜愛HIStory的觀眾們，我們在高雄舉辦為期三個月的主題限定店。\n店內將會提供限定餐飲、周邊商品等，還會特別還原戲中經典場景！準備讓大家拍照拍個夠、盡情滿足各位戲迷的願望！"))
        
        exhibitions.append(Exhibition(name: "漾藝廊 107年9-12月檔期公告", imageName_cover: "young_gallery_cover", imageName_square: "young_gallery_square", exhibitionDate: "2018-07-09~2018-12-31", openingTime: "每日12:00-18:00", ticketInfo: "免費", ticketDescription: "免費", location: "大義區C8-20", exhibitionInfo: "尚無資訊"))
        
        return ExhibitonCategory(category: .daYi, exhibitions: exhibitions)
    }
    
    private class func pongLaiExhibition() -> ExhibitonCategory {
        var exhibitions = [Exhibition]()
        
        exhibitions.append(Exhibition(name: "2018好漢玩字—好漢桃花源", imageName_cover: "han_letter_cover", imageName_square: "han_letter_square", exhibitionDate: "2018-11-01~2018-12-02", openingTime: "周一至周四10:00-18:00；\n周五至周日及國定假日10:00-20:00", ticketInfo: "本展一人一票，憑票入場。每張票券限一人使用一次。\n年齡12歲以下或身高150公分以下之幼童，與持身心障礙手冊及必要陪同者，請出示相關證明文件正本，得免票進場，敬老票一人79元，須現場出示相關證件使得購買。\n10/1-10/31 早鳥票 99元/人\n11/1-12/2 單人票149元/人。", ticketDescription: "售票", location: "駁二蓬萊B4倉庫", exhibitionInfo: "一窺潛藏於駁二的孟冬限定景點，以漢字為產物的神祕村落\n好漢桃花源，一座信仰文字的神祕聚落，漢字是這裡最重要的產物，村民無論士農工商都以玩字為業並且怡然自樂，在與世隔絕的一方天地間，創造了風情萬種的漢字面貌；若你也幸運遇見通往桃花源的那一道光，請捨棄對漢字的既定印象，循著陶淵明風光明媚的文句，展開一場漫遊於字裡行間的秘境之旅。"))
        
        exhibitions.append(Exhibition(name: "哈瑪星台灣鐵道館", imageName_cover: "hamahsing_cover", imageName_square: "hamahsing_square", exhibitionDate: "常設展", openingTime: "週一、三、四10:00-18:00(17:00最後售票進場)\n週五至週日營業時間應為10:00-19:00（18:30最後售票進場）\n（每週二休館）", ticketInfo: "愛PASS系統或至哈瑪星台灣鐵道館售票處\n◤購票可洽 https://www.ipasskhcc.tw/home/grades/109◢", ticketDescription: "售票", location: "駁二特區蓬萊B7、B8倉庫 (高雄市鼓山區蓬萊路99號)", exhibitionInfo: "百年前，日治時期臺灣總督府為確實治理臺灣，前瞻性地以打狗為鐵道縱貫線南端的起點，進而帶動日後填海造陸、濱線出現、打狗築港、貨物停車場、港區倉庫群以及後續的市街開發，為港都高雄奠定深厚根基。"))
        
        return ExhibitonCategory(category: .ponglai, exhibitions: exhibitions)
    }
    
    private class func experientExhibition() -> ExhibitonCategory {
        var exhibitions = [Exhibition]()
        return ExhibitonCategory(category: .experient, exhibitions: exhibitions)
    }
    
    private class func exhibition() -> ExhibitonCategory {
        var exhibitions = [Exhibition]()
        
        exhibitions.append(Exhibition(name: "攝影新時代—自拍狂潮", imageName_cover: "selfie_cover", imageName_square: "selfie_square", exhibitionDate: "2018-10-11~2019-01-06", openingTime: "周一至周四10:00-18:00\n周五至周日及國定假日10:00-20:00", ticketInfo: "一人一票，限同一人單日使用，展覽不限進場次數。 年齡6歲以下或身高115公分以下之幼童，與持身心障礙手冊及必要陪同者，請出示相關證明文件正本，得免票進場。\n2018高雄攝影節期10/11-10/28間\n早鳥票99元 全票149元（含P2、P3倉庫展區", ticketDescription: "售票", location: "駁二藝術特區大勇區C5倉庫", exhibitionInfo: "攝影新時代：「自拍」狂潮\nThe New Era of Photography：The Fever of “Self-portraits”\n這是一個用「自拍」來書寫歷史的時代。"))
        
        exhibitions.append(Exhibition(name: "駁二勇市集Pier-2 Market", imageName_cover: "young_market_cover", imageName_square: "young_market_cover", exhibitionDate: "2018-10-06~2018-11-25", openingTime: "每周末六日 13:00-18:00", ticketInfo: "~~~~~~~~~~~~~~免費~~~~~~~~~~~~~~", ticketDescription: "免費", location: "大勇區藝術廣場", exhibitionInfo: "［9927］手繪創意\n1+1=5手作雜貨\n1212玩樂設計\n2 Hands Waltz手作圓舞曲\nAbby's Illustration 插畫 x 手機殼訂製\nAlace 水蕾絲\nALMA の 手作人蔘\nAMIN'S SHINY WORLD\nBlack angel boutique\nBonjour！Bonheur 你好，幸福 手工天然果醬\nBubu＿studio 布布工作室\nCAT X KUMA\nciao metal design\nCOBE。手創原石\nCorotte等等各式商家"))
        
        exhibitions.append(Exhibition(name: "2018高雄藝術博覽會", imageName_cover: "art_kaohsiung_cover", imageName_square: "art_kaohsiung_square", exhibitionDate: "早鳥一日票 NT$ 100\n早鳥一日套票 NT$ 250\n團體樂透票 NT$ 510\n專刊乙本 NT$ 200", openingTime: "11/30-12/2毎日上午11:00至下午7:00，12/2\n開放至下午6:00", ticketInfo: "https://www.accupass.com/go/2018ak", ticketDescription: "售票", location: "駁二藝術特區大勇區P2、P3倉庫及城市商旅真愛館", exhibitionInfo: "ART KAOHSIUNG 高雄藝術博覽會於今年正式邁入第六屆,從 2013年開創以來,悉心耕耘台灣南部藝術市場及展業,已成功開啟台灣藝術市場新區塊。結合產官學三方力量,寫出南部藝文產業最新篇章。會展囊括中國、日本、菲律賓、香港、馬來西亞、新加坡、越南與美國近百間頂尖畫廊盛情參與。\n\n高雄藝術博覽會奠定出「東南亞及東北亞藝術的交會平台」的定位, 全力以台灣政府南進政策為地基,藝術文化交流作為前導,創立前瞻性的藝術收藏趨勢。經過六年的策略性運作及細心經營,2018年將會是高雄藝術博覽會重要的里程碑。結合高雄市政府文化局的政策遠見、各方專業經驗、網絡資源、尊榮藏家招待計畫與特別品牌聯名企劃,今年高雄藝術博覽會將展現前所未有的南部藝術實力。"))
        
        exhibitions.append(Exhibition(name: "伊藤潤二恐怖體驗展絕命逃走中 高雄站", imageName_cover: "chill_party_cover", imageName_square: "chill_party_square", exhibitionDate: "2018-12-29~2019-03-03", openingTime: "10:00~18:00（17:30最後入場）", ticketInfo: "＊單館票300元\n＊雙館票500元\n＊優待票150元（65歲以上長者、身心障礙人士本人(憑證)及陪同者一名，購票及入場時請出示相關證明文件）\n＊6歲以下幼童禁止入場；6~12歲兒童須購票，並須成人持票陪同入場。", ticketDescription: "售票", location: "大勇區自行車倉庫", exhibitionInfo: "亞洲首座重現「當代恐怖漫畫大師-伊藤潤二世界觀」的恐怖體驗展\n驚典篇章重啟新頁，再現高雄駁二！\n2018年底、跨年度，挑戰你最不想面對的感官體驗！\n\n\n【恐懼鎮、告別市，雙館齊嚇！】\n瀰漫絕望與不祥的恐懼鎮中，\n淵小姐以腥紅大嘴向你微笑，\n在巷弄盡頭等待你的，是從全身孔洞中流竄而出的絲絲寒氣；\n\n致命氣息充斥幽暗的告別市，\n雙一再度從閣樓裡敲響喪鐘，\n一道道不知通往何處的門扉，你找得到逃出這個城市的路嗎？\n\n不死不滅的絕世美人富江，邀你共享伊藤潤二漫畫的毛骨悚然。\n\n【無數相同面孔後的你，究竟是誰？】\n是要阻擋別人的窺伺？還是作為窺伺別人的保護罩？\n在雙一及富江臉孔後的，還是不是你自己？\n恐怖體驗的第一步就是戴上面具，絕命逃走的遊戲即將開始……\n\n【探索恐懼、了解恐懼，終將化身恐懼】\n與你深情對望的眼球草、經歷萬年時光沖刷的長夢結晶、女王姿態頤指氣使的富江人面疽、插滿邪氣怨念的雙一詛咒娃娃……，為你呈現伊藤潤二的世界中，所有恐懼源頭，讓驚嚇深入血脈，直達內心。"))
        
        exhibitions.append(Exhibition(name: "瘋狂泡泡實驗室 高雄站", imageName_cover: "bubble_cover", imageName_square: "bubble_square", exhibitionDate: "2018-12-29~2019-04-07", openingTime: "10:00~18:00（17:30最後入場）(除夕休館)", ticketInfo: "預售單人票220元，可至ibon售票系統、全網FamiPort、博客來售票網、GOMAJI平台購買。現場全票280元/優待票250元", ticketDescription: "售票", location: "大勇區P2倉庫", exhibitionInfo: "讓創意發泡 顛覆想像的泡泡樂園\n全台首創以泡泡為主題的情境式展覽，甩掉課本丟掉書包！科學原理簡單說！泡泡藝術超好玩！瘋狂泡泡實驗室打造大人與小孩同樂的科學藝術空間，從童趣好玩的泡泡延伸至背後的科學知識，除了擁有超好拍的夢幻打卡場景外，還有顛覆想像的泡泡樂園。\n各位科學家準備好了嗎？"))
        
        exhibitions.append(Exhibition(name: "鬼画連篇：臺灣動漫恐懼體驗展", imageName_cover: "ghost_cover", imageName_square: "ghost_square", exhibitionDate: "2018.9.1-12.16", openingTime: "周一至周四10:00-18:00；\n周五至周日及國定假日10:00-20:00", ticketInfo: "一人一票，限同一人單日使用，展覽不限進場次數。 年齡6歲以下或身高115公分以下之幼童，與持身心障礙手冊及必要陪同者，請出示相關證明文件正本，得免票進場。詳情請洽C5、C7售票點。\n《全日套票 - 99元，可當日無限次進出當代館、舊事倉庫及動漫倉庫展覽(鬼画連篇)\n單展票 - 50元，可單日單展無限次進出場館》", ticketDescription: "售票", location: "駁二藝術特區大義區C7動漫倉庫", exhibitionInfo: "展覽空間將有7道門，連結至漫畫家所創造的恐怖世界，恐懼題材涵蓋: 臺灣妖怪傳說、社會獵奇、女巫、凶宅、陰間、懸疑驚悚等，觀眾可探索7種不同的毛骨悚然體驗。"))
        
        exhibitions.append(Exhibition(name: "黑白畫話--哭泣的動物與人造動物", imageName_cover: "white_black_cover", imageName_square: "white_black_square", exhibitionDate: "2018-11-01~2018-11-24", openingTime: "周一至周日 12:00-18:00", ticketInfo: "~~~~~~~~~~~~~~免費~~~~~~~~~~~~~~", ticketDescription: "免費", location: "大義倉庫 C8-20漾藝廊", exhibitionInfo: "希望透過這系列畫中有話的黑白畫話，可以帶給大家對動物不同層面的思考，黑白更能呈現主角的情緒，就像黑白照片，更能聚焦。\n\n「哭泣的動物」是一系列以動物立場的創作，人類在受到傷害時會哭泣，哭泣會讓人產生同情和同理心，如果看到動物因為人類的傷害而哭泣，是否會願意給他們多點同理心？這些畫要表現的不是動物可愛，而是他們總被忽略的悲傷情緒。也藉這系列傳達拒絕皮草，抵制動物娛樂的理念。\n\n「人造動物」是畫未來人類與動物的想像。在未來人類殺光了大部分動物，只好製造機器動物，並在這些人造動物中加入了人類的喜好，讓動物明正言順的替人類服務，但人類小看了大自然的力量，這些動物體內像是有種子般發了芽長了樹枝，也因此被丟棄..."))
        
        exhibitions.append(Exhibition(name: "駁二舊事倉庫", imageName_cover: "antique_warehouse_cover", imageName_square: "antique_warehouse_square", exhibitionDate: "常設展", openingTime: "周一至周四10am~6pm，\n周五至周日及國定假日10am~8pm", ticketInfo: "一人一票，限同一人單日使用，展覽不限進場次數。\n年齡6歲以下或身高115公分以下之幼童，與持身心障礙手冊及必要陪同者，請出示相關證明文件正本，得免票進場。", ticketDescription: "售票", location: "駁二藝術特區大義C7倉庫", exhibitionInfo: "集結舊事鑑往知來──\n從福爾摩沙、打狗、鹽埕到駁二，自漁鹽、製糖、工業到藝文，將這一塊濱海地帶的人文歷史與核心價值，以各種創意手法濃縮在駁二「舊事倉庫」裡。以地洞影片為駁二歷史揭開序幕，帶我們回到三百年前的福爾摩沙小漁村，在海底洋流的空間氛圍中，打狗的發展一幕幕以新舊交融的影像出現眼前。"))
        
        exhibitions.append(Exhibition(name: "HIStory 主題設定店展", imageName_cover: "history_theme_cover", imageName_square: "history_theme_square", exhibitionDate: "2018-08-18~2018-11-30", openingTime: "週一-週日11:00-21:30", ticketInfo: "低消任一商品/每人", ticketDescription: "公告", location: "大義區C9-18倉庫", exhibitionInfo: "為了回饋所有喜愛HIStory的觀眾們，我們在高雄舉辦為期三個月的主題限定店。\n店內將會提供限定餐飲、周邊商品等，還會特別還原戲中經典場景！準備讓大家拍照拍個夠、盡情滿足各位戲迷的願望！"))
        
        exhibitions.append(Exhibition(name: "漾藝廊 107年9-12月檔期公告", imageName_cover: "young_gallery_cover", imageName_square: "young_gallery_square", exhibitionDate: "2018-07-09~2018-12-31", openingTime: "每日12:00-18:00", ticketInfo: "免費", ticketDescription: "免費", location: "大義區C8-20", exhibitionInfo: "尚無資訊"))
        
        exhibitions.append(Exhibition(name: "2018好漢玩字—好漢桃花源", imageName_cover: "han_letter_cover", imageName_square: "han_letter_square", exhibitionDate: "2018-11-01~2018-12-02", openingTime: "周一至周四10:00-18:00；\n周五至周日及國定假日10:00-20:00", ticketInfo: "本展一人一票，憑票入場。每張票券限一人使用一次。\n年齡12歲以下或身高150公分以下之幼童，與持身心障礙手冊及必要陪同者，請出示相關證明文件正本，得免票進場，敬老票一人79元，須現場出示相關證件使得購買。\n10/1-10/31 早鳥票 99元/人\n11/1-12/2 單人票149元/人。", ticketDescription: "售票", location: "駁二蓬萊B4倉庫", exhibitionInfo: "一窺潛藏於駁二的孟冬限定景點，以漢字為產物的神祕村落\n好漢桃花源，一座信仰文字的神祕聚落，漢字是這裡最重要的產物，村民無論士農工商都以玩字為業並且怡然自樂，在與世隔絕的一方天地間，創造了風情萬種的漢字面貌；若你也幸運遇見通往桃花源的那一道光，請捨棄對漢字的既定印象，循著陶淵明風光明媚的文句，展開一場漫遊於字裡行間的秘境之旅。"))
        
        exhibitions.append(Exhibition(name: "哈瑪星台灣鐵道館", imageName_cover: "hamahsing_cover", imageName_square: "hamahsing_square", exhibitionDate: "常設展", openingTime: "週一、三、四10:00-18:00(17:00最後售票進場)\n週五至週日營業時間應為10:00-19:00（18:30最後售票進場）\n（每週二休館）", ticketInfo: "愛PASS系統或至哈瑪星台灣鐵道館售票處\n◤購票可洽 https://www.ipasskhcc.tw/home/grades/109◢", ticketDescription: "售票", location: "駁二特區蓬萊B7、B8倉庫 (高雄市鼓山區蓬萊路99號)", exhibitionInfo: "百年前，日治時期臺灣總督府為確實治理臺灣，前瞻性地以打狗為鐵道縱貫線南端的起點，進而帶動日後填海造陸、濱線出現、打狗築港、貨物停車場、港區倉庫群以及後續的市街開發，為港都高雄奠定深厚根基。"))
        
        return ExhibitonCategory(category: .exhibition, exhibitions: exhibitions)
    }
    
    private class func marketExhibition() -> ExhibitonCategory {
        var exhibitions = [Exhibition]()
        
        exhibitions.append(Exhibition(name: "駁二勇市集Pier-2 Market", imageName_cover: "young_market_cover", imageName_square: "young_market_cover", exhibitionDate: "2018-10-06~2018-11-25", openingTime: "每周末六日 13:00-18:00", ticketInfo: "~~~~~~~~~~~~~~免費~~~~~~~~~~~~~~", ticketDescription: "免費", location: "大勇區藝術廣場", exhibitionInfo: "［9927］手繪創意\n1+1=5手作雜貨\n1212玩樂設計\n2 Hands Waltz手作圓舞曲\nAbby's Illustration 插畫 x 手機殼訂製\nAlace 水蕾絲\nALMA の 手作人蔘\nAMIN'S SHINY WORLD\nBlack angel boutique\nBonjour！Bonheur 你好，幸福 手工天然果醬\nBubu＿studio 布布工作室\nCAT X KUMA\nciao metal design\nCOBE。手創原石\nCorotte等等各式商家"))
        
        return ExhibitonCategory(category: .daYoung, exhibitions: exhibitions)
    }
}


