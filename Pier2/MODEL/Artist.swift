//
//  Artist.swift
//  Pier2
//
//  Created by yang on 2018/12/11.
//  Copyright © 2018 yang. All rights reserved.
//

import MapKit
import AddressBook

class Artist: NSObject, MKAnnotation{
    
    var artistImage:UIImage
    var artistName:String
    var coordinate: CLLocationCoordinate2D
    var engAddress:String
    
    init(artistImageName:String, artistName:String, coordinate: CLLocationCoordinate2D, engAddress:String) {
        
        if let image = UIImage(named: artistImageName) {
            self.artistImage = image
        }else {
            self.artistImage = UIImage(named: "Default")!
        }
        
        self.artistName = artistName
        self.coordinate = coordinate
        self.engAddress = engAddress
        
        super.init()
    }
    
    var title: String? {
        return artistName
    }
    
    var subtitle: String? {
        return engAddress
    }
}
