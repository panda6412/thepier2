//
//  ArtistLibrary.swift
//  Pier2
//
//  Created by yang on 2018/12/11.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit
import MapKit

class ArtistLibrary {
    
    class func getArtists() -> [Artist] {
        
        var artists:[Artist] = [Artist]()
        artists.append(Artist(artistImageName: "artist_1", artistName: "神隱神現", coordinate: CLLocationCoordinate2D(latitude: 22.621427, longitude: 120.283341), engAddress: "Unnamed Road, Yancheng District, Kaohsiung City, 803"))
        
        artists.append(Artist(artistImageName: "artist_2", artistName: "在海平面折返的視線", coordinate: CLLocationCoordinate2D(latitude: 22.621278, longitude: 120.283726), engAddress: "No. 124, Gongyuan 2nd Road, Yancheng District, Kaohsiung City, 803"))
        
        
        artists.append(Artist(artistImageName: "artist_3", artistName: "火車", coordinate: CLLocationCoordinate2D(latitude: 22.618358, longitude: 120.286976), engAddress: "Yancheng District, Kaohsiung City, 803"))
        
        
        artists.append(Artist(artistImageName: "artist_4", artistName: "停車再開", coordinate: CLLocationCoordinate2D(latitude: 22.618883, longitude: 120.287394), engAddress: "Yancheng District, Kaohsiung City, 803"))
        
        
        artists.append(Artist(artistImageName: "artist_5", artistName: "喇叭", coordinate: CLLocationCoordinate2D(latitude: 22.618328, longitude: 120.288016), engAddress: "Yancheng District, Kaohsiung City, 803"))
        
        artists.append(Artist(artistImageName: "artist_6", artistName: "旅行箱", coordinate: CLLocationCoordinate2D(latitude: 22.619625, longitude: 120.286814), engAddress: "Gongyuan 2nd Road, Yancheng District, Kaohsiung City, 803"))
        
        artists.append(Artist(artistImageName: "artist_7", artistName: "Keep Clean", coordinate: CLLocationCoordinate2D(latitude: 22.619546, longitude: 120.287619), engAddress: "No. 8, Lane 40, Gongyuaner Road, Yancheng District, Kaohsiung City, 803"))
        
        artists.append(Artist(artistImageName: "artist_8", artistName: "遊歷", coordinate: CLLocationCoordinate2D(latitude: 22.618011, longitude: 120.288198), engAddress: "Unnamed Road, Yancheng District, Kaohsiung City, 803"))
        
        
        artists.append(Artist(artistImageName: "artist_9", artistName: "火車", coordinate: CLLocationCoordinate2D(latitude: 22.618863, longitude: 120.287833), engAddress: "Yancheng District, Kaohsiung City, 803"))
        
        artists.append(Artist(artistImageName: "artist_10", artistName: "石頭", coordinate: CLLocationCoordinate2D(latitude: 22.619198, longitude: 120.288251), engAddress: "Unnamed Road, Yancheng District, Kaohsiung City, 803"))
        
        return artists
    }
}
