//
//  Exhibition.swift
//  Pier2
//
//  Created by yang on 2018/11/25.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class Exhibition {
    var name:String
    var image_cover:UIImage
    var image_square:UIImage
    var exhibiteDate:String
    var openingTime:String
    var ticketInfo:String
    var ticketDescription:String
    var location:String
    var exhibitionInfo:String
    var stroed:Bool
    
    init(name: String, imageName_cover:String, imageName_square:String, exhibitionDate:String, openingTime:String, ticketInfo:String, ticketDescription:String, location:String, exhibitionInfo:String) {
        self.name = name
        
        if let imageCover = UIImage(named: imageName_cover) {
            self.image_cover = imageCover
        }else{
            self.image_cover = UIImage(named: "Default")!
        }
        
        if let imageSquare = UIImage(named: imageName_square) {
            self.image_square = imageSquare
        }else{
            self.image_square = UIImage(named: "Default")!
        }
        
        self.exhibiteDate = exhibitionDate
        self.openingTime = openingTime
        self.ticketInfo = ticketInfo
        self.ticketDescription = ticketDescription
        self.location = location
        self.exhibitionInfo = exhibitionInfo
        self.stroed = false
    }
}
