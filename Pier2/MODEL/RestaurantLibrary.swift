//
//  RestaurantLibrary.swift
//  Pier2
//
//  Created by yang on 2018/12/2.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class RestaurantLibrary {
    class func getRestaurantList() -> [Restaurant] {
        var restaurants = [Restaurant]()
        restaurants.append(Restaurant(name: "微熱山丘C11", engName: "Sun & Hill", restaurantCoverImageName:"sunhill", category: [.dessert], opendate: "每日11:00-19:00", restaurantInfo: "微熱山丘與南台灣的首次相遇--駁二藝術特區，融合在地特色，船舶造型的獨特外觀、綠地為伴的開放空間、友善簡約的內部設計，結合駁二獨有的文藝氣息，展現出南臺灣專屬的「微熱」風情。在這裡，我們以微熱山丘經典樸實的美味糕點，恰如其分地傳達著那份靦腆中隱含著熱情的待客之道。", phone: "07-5510558", location: "803, Kaohsiung City, Yancheng District, Dayi Street, 2-6號C11-1倉庫"))
        
        restaurants.append(Restaurant(name: "Pacini 派奇尼義式冰淇淋C8", engName: "Pacini", restaurantCoverImageName:"pacini", category: [.dessert], opendate: "週一、三、四　12:00-18:00\n週五、六、日　12:00-19:00", restaurantInfo: "Pacini gelati e dolci 派奇尼義式冰淇淋。\n異國文化 X 義式生活 X 時尚現代\n堅持．道地．新鮮：挖一口嚐盡全世界的新奇。\n愉悅，TAKE AWAY！", phone: "07-5212201", location: "No. 2, Warehouse C8-16, Dayi Street, Yancheng District, Kaohsiung City, 803"))
        
        restaurants.append(Restaurant(name: "典藏駁二餐廳C6", engName: "Artco.c6", restaurantCoverImageName:"artco.c6", category: [.dinner, .brunch], opendate: "週一至週日 上午11:30至下午22:00", restaurantInfo: "結合藝術概念與美食佳餚\n休憩 × 漫步 × 觀景 × 駁二新指標\n延續典藏餐飲所標榜之「結合美食與藝術的饗宴」，融合時尚人文與歐式華麗風情的典藏駁二餐廳，挑高的設計呈現空間視覺的美感。店內的擺飾無不充滿巧思，精緻中流露出輕鬆自在，奢華中帶點復古，舒適優雅的用餐環境與藝文氣息，令人心曠神怡。", phone: "(07) 521-1936", location: "No. 2, Dayi Street, Yancheng District, Kaohsiung City, 803"))
        
        restaurants.append(Restaurant(name: "帕莎蒂娜餐廳C3", engName: "Pasadena", restaurantCoverImageName:"pasadena", category: [.dessert], opendate: "週二至週五 Tue to Fri 10:30-21:30\n週六至週日 Sat to Sun 10:00－21:30", restaurantInfo: "帕莎蒂娜駁二倉庫餐廳的前身是85年以上歷史的老糖倉，保留了紅磚、水泥牆和檜木屋頂結構，呈現半世紀前的時代軌跡；2012年帕莎蒂娜以美食、藝文為老糖倉再造新生命，提供融合美式與歐式料理的手工窯烤比薩、漢堡、燒烤、新鮮啤酒，為了呼應糖倉歷史，更推出獨家1928黑糖披薩和霜淇淋，適合三五好友或家族出遊一同來此用餐來餐廳吃美食吧!", phone: "(07)531-1106", location: "No. 1, Dayong Road, Yancheng District, Kaohsiung City, 803"))
        
        restaurants.append(Restaurant(name: "iCE+艾司加冰屋C5", engName: "Ice", restaurantCoverImageName:"ice", category: [.dessert], opendate: "周一到周四10:00-18:00\n周五到周日及國定假日10:00-20:00", restaurantInfo: "Less is More歸零，才有真滋味！零下冰點的「雪花司諾」，零人工色素、零人工乳化劑、零化學香料、零果糖、零防腐劑添加。讓舌尖感受歸零後的原味！\nSimple is Better純粹、自然、原味！以純粹的心意，用純粹的原料，做出純粹的冰品，所以我們的冰品有時會融化的快一些, 顏色偶爾不太漂亮, 香味也不太明顯, 只為讓大家嚐到最純粹的美味！", phone: "07-777-7752", location: "No. 491, Wenheng Road, Fengshan District, Kaohsiung City, 830"))
        
        restaurants.append(Restaurant(name: "掌門精釀啤酒-義法料理餐廳", engName: "Zhangmenbrewing", restaurantCoverImageName:"zhangmenbrewing", category: [.lunch,.beer], opendate: "11:00~22:00", restaurantInfo: "提供36種在地精釀啤酒，搭配風味絕佳義法料理，以及180°全視角無敵海景，要讓在地高雄人與異地旅客均能獲得賓至如歸的味覺饗宴。", phone: "07 551 7799", location: "No. 17號, Penglai Road, Gushan District, Kaohsiung City, 804"))
        
        restaurants.append(Restaurant(name: "卡滋嗑", engName: "Caseker", restaurantCoverImageName:"caseker", category: [.dessert], opendate: "週日至週五 10:00~21:00\n週六(及國定假日) 10:00~22:00", restaurantInfo: "卡滋卡滋酥脆的的聲音，是炸雞好吃的第一關鍵，而第二關鍵就是讓你忍不住用雙手拿起大口大口的嗑，這就是「卡滋」「嗑」炸雞的由來。\n「卡滋嗑炸雞」採用自行研發的炸雞醃料、試煉出掌控美味火候的關鍵，\n成就「1脆2嫩3多汁」的美味炸雞，激發你對美味大快朵頤的真性情。", phone: "(07)531-3805", location: "No. 17號, Penglai Road, Gushan District, Kaohsiung City, 804"))
        
        restaurants.append(Restaurant(name: "香茗茶行", engName: "Tea", restaurantCoverImageName:"tea", category: [.dessert, .beverage], opendate: "10:00 ~ 21:00", restaurantInfo: "『簡單。天然。原味』\n是我們呈現茶飲的宗旨\n沒有添加\n沒有花招\n沒有調味\n一切以樸實的古早味為出發\n秉持傳統做工，才能呈現最好！", phone: "(07)5362718", location: "No. 264, Wufu 4th Road, Yancheng District, Kaohsiung City, 803"))
        
        restaurants.append(Restaurant(name: "PAO PAO FUN", engName: "Paopaofun", restaurantCoverImageName:"paopaofun", category: [.dessert], opendate: "10:00–21:00", restaurantInfo: "ＰＡＯ ＰＡＯ ＦＵＮ的生活態度、品味美學：「一點機緣、一點嘗試、一點時間、一點用心、對的器具，享受美好人生。」\n用簡單的飲食，傳達細膩的感受；良好的器具，體驗生活玩味；ＦＵＮ-D出你的生活態度，串起你的生命熱情。", phone: "07-531 8568", location: "804, Kaohsiung City, Gushan District, 蓬萊路17號 棧二庫1F—東側入口處 2107櫃"))
        
        restaurants.append(Restaurant(name: "貝力岡法式冰淇淋", engName: "Le Pelican", restaurantCoverImageName:"lePelican", category: [.dessert], opendate: "10:00-21:00", restaurantInfo: "南法主廚親赴台灣\n耗時兩年調配最道地的法式冰淇淋，對法國人來說，甜點是生活、文化、信仰 。\n法國人對甜點的堅持是從小就開始培養的挑剔味蕾。法國主廚Claude Barbe 親赴台灣，花了兩年的時間利用台灣在地水果、鮮奶等食材鹽發，最終打造出最精緻、無可挑剔、正宗的法式冰淇淋。", phone: "07-5318568 ", location: "No. 17號, Penglai Road, Gushan District, Kaohsiung City, 804"))
        
        restaurants.append(Restaurant(name: "ㄇㄚˊ幾兔主題餐廳_棧貳店", engName: "MACHIKO KITCHEN", restaurantCoverImageName:"MACHIKO_KITCHEN", category: [.dessert], opendate: "週日至週四 10:00-21:00\n週五與週六 10:00-22:00", restaurantInfo: "棧貳庫為高雄香蕉出口專用倉庫，為民國40、50年代的建築，為台灣經濟奇蹟創下輝煌的歷史，對於高雄舊港區的發展具有歷史意義。 現在我們秉持著高雄式左轉的精神乘著「ㄇㄚˊ幾號」，帶來美味的火鍋料理外，更在每道菜色中加上「療癒」，滿足味蕾的同時，也溫暖了心靈。", phone: "07-531-3222", location: "No. 17號, Penglai Road, Gushan District, Kaohsiung City, 804"))
        
        restaurants.append(Restaurant(name: "海斯頓法式甜點", engName: "Herston", restaurantCoverImageName:"herston", category: [.dessert], opendate: "週日至週四 10:00-21:00/週五與週六 10:00-22:00", restaurantInfo: "以馬卡龍為主角商品從高雄鳳山的小巷弄裡出發的小品牌。對於 喜歡海洋的“海斯頓”能在大海旁設立第一個門市更能表達海斯頓對於高雄海港的情感以及品牌的核心價值“深根高雄”的意義希望未來高雄這片土地能讓繽紛的馬卡龍點綴出法國美味的色彩。", phone: "07-531-8568", location: "830高雄市鳳山區文中街111號"))
        
        restaurants.append(Restaurant(name: "花甜果室", engName: "BLOSSOMINGJUICE", restaurantCoverImageName:"BLOSSOMINGJUICE", category: [.dessert, .beverage], opendate: "週一至週日 10:00-21:00\n週六 10:00-22:00", restaurantInfo: "花甜果室以提供多變化性的蔬果飲品及冰品為主軸，並混搭各式西餐概念的有趣食材，創造富有變化性及創意能量的爽口飲食體驗，為你我的日常生活注入滿滿元氣。", phone: "07-531-2317", location: "No. 17號, Penglai Road, Gushan District, Kaohsiung City, 804"))
        
        restaurants.append(Restaurant(name: "路易莎咖啡", engName: "Louisa Coffee", restaurantCoverImageName:"louisaCoffee", category: [.coffee], opendate: "週一至週日 10:00-21:00", restaurantInfo: "路易莎咖啡是一個年輕、有活力的專業精品咖啡連鎖品牌，除了供應獨家經典配方豆以外，路易莎全省門市也提供來自全球多個國家地區，多達數十種的莊園級咖啡豆，而且是以親民的平實價格供應，讓路易莎的消費顧客可以在輕鬆無負擔的情形下享受不同精品咖啡的風味。而棧貳庫分店也是路易莎咖啡第一家在海港旁的門市。", phone: "07-531-0660", location: "804, Kaohsiung City, Gushan District, Penglai Road, 17號棧貳庫"))
        
        return restaurants
    }
}
