//
//  Store.swift
//  Pier2
//
//  Created by yang on 2018/12/3.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class Store {
    var storeName:String
    var storeEngName:String
    var storeImage:UIImage
    var location:String
    var openingTime:String
    var phone:String
    var couldPhoto:Bool = false
    var hasGratitude:Bool = false
    var hasPackage:Bool = false
    var couldTakePet:Bool = false
    var couldUseCredictCard:Bool = false
    var couldUseEpay:Bool = false
    var storeInfo:String
    var officialLink:String
    var fbLink:String
    
    init(storeName:String, storeEngName:String, storeImageName:String, location:String, openingTime:String, phone:String, couldPhoto:Bool, hasGratitude:Bool, hasPackage:Bool, couldTakePet:Bool, couldUseCredictCard:Bool,couldUseEpay:Bool, storeInfo:String, officialLink:String, fbLink:String) {
        self.storeName = storeName
        self.storeEngName = storeEngName
        
        if let image = UIImage(named: storeImageName) {
            self.storeImage = image
        }else {
            self.storeImage = UIImage(named: "Default")!
        }
        
        self.location = location
        self.openingTime = openingTime
        self.phone = phone
        self.couldPhoto = couldPhoto
        self.hasGratitude = hasGratitude
        self.hasPackage = hasPackage
        self.couldTakePet = couldTakePet
        self.couldUseCredictCard = couldUseCredictCard
        self.couldUseEpay  = couldUseEpay
        self.storeInfo = storeInfo
        self.officialLink = officialLink
        self.fbLink = fbLink
    }
}
