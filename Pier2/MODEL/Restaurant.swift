//
//  Restaurant.swift
//  Pier2
//
//  Created by yang on 2018/12/2.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

enum RestaurantCategory:String {
    case pasta = "Pasta"
    case beverage = "Beverage"
    case lunch = "Lunch"
    case brunch = "Brunch"
    case dinner = "Dinner"
    case dessert = "Dessert"
    case coffee = "Coffee"
    case beer = "Beer"
}
    
    
class Restaurant {
    var name:String
    var engName:String
    var restaurantCoverImage:UIImage!
    var category:[RestaurantCategory]
    var opendate:String
    var restaurantInfo:String
    var phone:String
    var location:String
    var morePhotos:[UIImage]?
    var mayYouWannaGo:[Restaurant]?

    init(name: String, engName:String, restaurantCoverImageName:String,category:[RestaurantCategory], opendate:String, restaurantInfo:String, phone:String, location:String) {
        self.name = name
        self.engName = engName
        
        if let imageCover = UIImage(named: restaurantCoverImageName) {
            self.restaurantCoverImage = imageCover
        }else{
            self.restaurantCoverImage = UIImage(named: "Default")!
        }
        
        self.category = category
        self.opendate = opendate
        self.restaurantInfo = restaurantInfo
        self.phone = phone
        self.location = location
    }   
}
