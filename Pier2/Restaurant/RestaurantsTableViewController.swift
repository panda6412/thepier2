//
//  RestaurantsTableViewController.swift
//  Pier2
//
//  Created by yang on 2018/12/3.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class RestaurantsTableViewController: UITableViewController {

    let restaurants:[Restaurant] = RestaurantLibrary.getRestaurantList()
    
    struct StoryBoard {
        static let showRestaurantDetailVC = "showRestaurantDetailVC"
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(restaurants)
        return restaurants.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantCell", for: indexPath) as! RestaurantTableViewCell
        let restaurant = restaurants[indexPath.row]
        cell.restaurant = restaurant
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    var selectedRestaurant: Restaurant?
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let restaurant = restaurants[indexPath.row]
        selectedRestaurant = restaurant
        
        performSegue(withIdentifier: StoryBoard.showRestaurantDetailVC, sender: nil)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == StoryBoard.showRestaurantDetailVC {
            let restaurantDetailViewVC = segue.destination as! RestaurantDetailTableViewController
            restaurantDetailViewVC.restaurant = selectedRestaurant
        }
    }
}
