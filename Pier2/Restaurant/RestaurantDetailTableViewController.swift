//
//  RestaurantDetailTableViewController.swift
//  Pier2
//
//  Created by yang on 2018/12/3.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit
import MapKit

class RestaurantDetailTableViewController: UITableViewController {
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var engNameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UIStackView!
    @IBOutlet weak var openTimeTextView: UITextView!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var infoTextView: UITextView!
    @IBOutlet weak var locationMap: MKMapView!
    
    var restaurant: Restaurant?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigation()
        
        self.coverImage.image = restaurant?.restaurantCoverImage
        self.nameLabel.text = restaurant?.name
        self.engNameLabel.text = restaurant?.engName
        self.openTimeTextView.text = restaurant?.opendate
        self.phoneButton.setImage(#imageLiteral(resourceName: "phone-1"), for: .normal)
        self.phoneButton.setTitle(restaurant?.phone, for: .normal)
        self.infoTextView.text = restaurant?.restaurantInfo
        
        if let location = restaurant?.location {
            pinOnMap(with: location)
        }
    }
    
    private func pinOnMap(with location: String) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location, completionHandler: { placemarks, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if let placemarks = placemarks {
                let placemark = placemarks[0]
                
                let annotation = MKPointAnnotation()
                
                if let location = placemark.location {
                    annotation.coordinate = location.coordinate
                    let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 250, longitudinalMeters: 250)
                    self.locationMap.addAnnotation(annotation)
                    self.locationMap.setRegion(region, animated: true)
                }
            }
        })
    }
    
    private func configureNavigation() {
        if let navigationBar = navigationController?.navigationBar {
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
            navigationBar.tintColor = .white
        }
        navigationController?.hidesBarsOnSwipe = true
        tableView.contentInsetAdjustmentBehavior = .never
    }
}
