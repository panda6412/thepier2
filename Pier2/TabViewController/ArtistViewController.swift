//
//  TestingViewController.swift
//  Pier2
//
//  Created by yang on 2018/12/9.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit
import MaterialComponents.MDCTabBar

class ArtistViewControllr: UIViewController {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    fileprivate lazy var artistMapTableViewController: ArtistMapTableViewController = self.buildFromStoryboard("Main")
    fileprivate lazy var artistListViewController: ArtistListTableViewController = self.buildFromStoryboard("Main")
    
    override func viewDidLoad() {
        let tabBar = MDCTabBar(frame: view.bounds)
        tabBar.items = [
            UITabBarItem(title: "Map Mode", image: UIImage(named: "map"), tag: 0),
            UITabBarItem(title: "List Mode", image: UIImage(named: "list"), tag: 0),
        ]
        tabBar.itemAppearance = .images
        tabBar.setTitleColor(UIColor.gray, for: .normal)
        tabBar.setTitleColor(UIColor.black, for: .selected)
        tabBar.alignment = .center
        tabBar.delegate = self
        tabBar.translatesAutoresizingMaskIntoConstraints = true
        tabBar.sizeThatFits(CGSize(width: headerView.bounds.width, height: headerView.bounds.height))
        headerView.addSubview(tabBar)
        
        addContentController(artistMapTableViewController, to: contentView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func addContentController(_ child: UIViewController, to view: UIView) {
        
        addChild(child)
        view.addSubview(child.view)
//        stackView.addArrangedSubview(child.view)
        child.didMove(toParent: self)
    }
    
    private func removeContentController(_ child: UIViewController, from view: UIView) {
        
        child.willMove(toParent: nil)
        view.removeFromSuperview()
//        stackView.removeArrangedSubview(child.view)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
    private func buildFromStoryboard<T>(_ name: String) -> T {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let identifier = String(describing: T.self)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
            fatalError("Missing \(identifier) in Storyboard")
        }
        return viewController
    }
}

extension ArtistViewControllr: MDCTabBarDelegate {
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        print("selected item is \(item.title!)")
        switch item.title! {
        case "Map Mode":
            addContentController(artistMapTableViewController, to: contentView)
            
        case "List Mode":
            addContentController(artistListViewController, to: contentView)
            
        default:
            print("nothing selected!")
            
        }
    }
}
