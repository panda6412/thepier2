//
//  ExhibitionViewController.swift
//  Pier2
//
//  Created by yang on 2018/11/22.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class ExhibitionViewController: UIViewController {

    @IBOutlet var exhibition_gallery:ExhibitionUIView!
    override func viewDidLoad() {
        super.viewDidLoad()

//        exhibition_gallery.setGradientBackground(colorTop: UIColor(hexString: "#FFFFFF"),colorBottom: UIColor(hexString: "#C7C7C7"))
        
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowImagesPageViewController" {
            if let imagesPageVC = segue.destination as? ExhibitionImagesPageViewController {
                imagesPageVC.pageViewControllerDelegate = exhibition_gallery
            }
        }
    }


}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
