//
//  StoreDetailTableViewController.swift
//  Pier2
//
//  Created by yang on 2018/12/4.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit
import MapKit

class StoreDetailTableViewController: UITableViewController {
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var storeEngNameLabel: UILabel!
    @IBOutlet weak var openingTimeTextView: UITextView!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var storeInfoTextView: UITextView!
    @IBOutlet weak var couldPhoto: UIImageView!
    @IBOutlet weak var couldTakePet: UIImageView!
    @IBOutlet weak var hasGratitude: UIImageView!
    @IBOutlet weak var hasCredictCard: UIImageView!
    @IBOutlet weak var couldPackage: UIImageView!
    @IBOutlet weak var couldUseEpay: UIImageView!
    @IBOutlet weak var locationMap: MKMapView!
    
    var store:Store?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigation()
        updateUI()
    }
    
    private func updateUI() {
        self.coverImage.image = store?.storeImage
        self.storeNameLabel.text = store?.storeName
        self.storeEngNameLabel.text = store?.storeEngName
        self.openingTimeTextView.text = store?.openingTime
        self.phoneButton.setImage(#imageLiteral(resourceName: "phone-1"), for: .normal)
        self.phoneButton.setTitle(store?.phone, for: .normal)
        self.storeInfoTextView.text = store?.storeInfo
        self.couldPhoto.image = getAllowIcon(from: (store?.couldPhoto)!)
        self.couldTakePet.image = getAllowIcon(from: (store?.couldTakePet)!)
        self.hasGratitude.image = getAllowIcon(from: (store?.hasGratitude)!)
        self.hasCredictCard.image = getAllowIcon(from: (store?.hasGratitude)!)
        self.couldPackage.image = getAllowIcon(from: (store?.hasPackage)!)
        self.couldUseEpay.image = getAllowIcon(from: (store?.couldUseEpay)!)
        
        if let location = store?.location {
            pinOnMap(with: location)
        }
    }
    
    private func getAllowIcon(from bool:Bool) -> UIImage? {
        let imageName =  bool ? "allow" : "deny"
        return UIImage(named: imageName)
    }
    
    private func pinOnMap(with location: String) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location, completionHandler: { placemarks, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if let placemarks = placemarks {
                let placemark = placemarks[0]
                
                let annotation = MKPointAnnotation()
                
                if let location = placemark.location {
                    annotation.coordinate = location.coordinate
                    let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 250, longitudinalMeters: 250)
                    self.locationMap.addAnnotation(annotation)
                    self.locationMap.setRegion(region, animated: true)
                }
            }
        })
    }
    
    
    private func configureNavigation() {
        if let navigationBar = navigationController?.navigationBar {
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
            navigationBar.tintColor = .white
            
            tableView.contentInsetAdjustmentBehavior = .never
        }
    }

}
