//
//  StoresTableViewController.swift
//  Pier2
//
//  Created by yang on 2018/12/3.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class StoresTableViewController: UITableViewController {

    var stores = StoreLibrary.getStores()
    
    struct Storyboard {
        static let showStoreDetailVC = "showStoreDetailVC"
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores.count
    }
 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! StoreTableViewCell
        cell.store = stores[indexPath.row]
        return cell
    }
    
    var selectedStore:Store?
    // MARK : - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let store = stores[indexPath.row]
        selectedStore = store
        performSegue(withIdentifier: Storyboard.showStoreDetailVC, sender: nil)
    }
    
    // MARK : - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Storyboard.showStoreDetailVC {
            let storeDetailVC = segue.destination as! StoreDetailTableViewController
            storeDetailVC.store = selectedStore
        }
    }
}
