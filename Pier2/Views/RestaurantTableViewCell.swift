//
//  RestaurantTableViewCell.swift
//  Pier2
//
//  Created by yang on 2018/12/3.
//  Copyright © 2018 yang. All rights reserved.
//
import Foundation
import UIKit

class RestaurantTableViewCell: UITableViewCell {
    @IBOutlet weak var theImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var engNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var categoryLabels: UIStackView!
    
    var restaurant:Restaurant! {
        didSet {
            self.updateUI()
        }
    }
    
    private func updateUI() {
        
        initStyle()
        
        self.theImageView.image = restaurant.restaurantCoverImage
        self.nameLabel.text = restaurant.name
        self.engNameLabel.text = restaurant.engName
        
        
//        self.distanceLabel.text = caculateDistance(from: restaurant.location)
    }
    
    private func initStyle(){
        self.theImageView.layer.cornerRadius = self.theImageView.bounds.width/2
        self.theImageView.layer.masksToBounds = true
    }
    
    private func caculateDistance(from location:String) -> String {
        return  "125m"
    }

}
