//
//  File.swift
//  Pier2
//
//  Created by yang on 2018/12/1.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class ExhibitionCell : UICollectionViewCell {
    @IBOutlet weak var exhibitionImageView: UIImageView!
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var infoDescription: UILabel!
    
    var exhibitionImage:UIImage! {
        didSet{
            exhibitionImageView.image = exhibitionImage
        }
    }
    var infoImage:UIImage! {
        didSet{
            infoImageView.image = infoImage
        }
    }
    var infoDescriptionStr:String! {
        didSet{
            infoDescription.text = infoDescriptionStr
        }
    }
}
