 //
//  SectionHeaderView.swift
//  Pier2
//
//  Created by yang on 2018/12/1.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

 class SectionHeaderView: UICollectionReusableView {
    @IBOutlet weak var title: UILabel!
    
    var catgoryTitle:String! {
        didSet {
            title.text = catgoryTitle
        }
    }
 }
