//
//  ArtistTableViewCell.swift
//  Pier2
//
//  Created by yang on 2018/12/13.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class ArtistTableViewCell: UITableViewCell {
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var engAddress: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var artist:Artist! {
        didSet {
            self.updateUI()
        }
    }
    
    private func updateUI() {
        
        initStyle()
        
        self.artistImage.image = artist.artistImage
        self.artistName.text = artist.artistName
        self.engAddress.text = artist.engAddress
        self.distanceLabel.text = "120m"
    }
    
    private func initStyle() {
        self.artistImage.layer.cornerRadius = self.artistImage.bounds.width/2
        self.artistImage.layer.masksToBounds = true
    }

}
