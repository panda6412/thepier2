//
//  StoreDetailTableViewCell.swift
//  Pier2
//
//  Created by yang on 2018/12/3.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class StoreTableViewCell: UITableViewCell {
    @IBOutlet weak var theImageView: UIImageView!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var storeEngName: UILabel!
    
    var store:Store! {
        didSet {
            self.updateUI()
        }
    }
    
    private func updateUI() {
        initStyle()
        
        self.theImageView.image = store.storeImage
        self.storeName.text = store.storeName
        self.storeEngName.text = store.storeEngName
    }
    
    private func initStyle() {
        self.theImageView.layer.cornerRadius = self.theImageView.bounds.width/10
        self.theImageView.layer.masksToBounds = true
    }
    
}
