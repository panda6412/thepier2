//
//  AboutTableViewController.swift
//  Pier2
//
//  Created by yang on 2018/12/15.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit

class AboutTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myIndex =  indexPath.row
        switch myIndex {
            case 1:
                performSegue(withIdentifier: "knowMe", sender: self)
            case 2:
                performSegue(withIdentifier: "transportation", sender: self)
            case 3:
                performSegue(withIdentifier: "tellYouASecret", sender: self)
            default:
                print("nothing..")
        }
    }
}
