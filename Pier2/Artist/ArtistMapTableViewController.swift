//
//  ArtistMapTableViewController.swift
//  Pier2
//
//  Created by yang on 2018/12/9.
//  Copyright © 2018 yang. All rights reserved.
//

import UIKit
import MapKit

class ArtistMapTableViewController: UITableViewController {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistAddress: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var artists:[Artist] = ArtistLibrary.getArtists()
    override func viewDidLoad() {
        super.viewDidLoad()
        let initLocation = CLLocation(latitude: 22.619198, longitude: 120.288251)
        zoomMapOn(location: initLocation)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationServiceAuthenticationStatus()
    }
    
    private let regionRadius: CLLocationDistance = 1000
    
    func zoomMapOn(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        
        mapView.delegate = self
        mapView.setRegion(coordinateRegion, animated: true)
        mapView.addAnnotations(artists)
    }
    
    var locationManager = CLLocationManager()
    
    func checkLocationServiceAuthenticationStatus() {
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }

}

extension ArtistMapTableViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.mapView.showsUserLocation = true
//        let location = locations.last!
        print(locations)
//        zoomMapOn(location: location)
    }
}

extension ArtistMapTableViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Artist {
            var view: MKPinAnnotationView
            if let dequeView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") as? MKPinAnnotationView{
                dequeView.annotation = annotation
                view = dequeView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            return view
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Artist
        print(location)
        artistImage.image = location.artistImage
        artistName.text = location.artistName
        artistAddress.text = location.engAddress
        distanceLabel.text = "111m"
    }
    
}
